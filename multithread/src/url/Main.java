package url;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = null;
        URL url = new URL("https://api.coinmarketcap.com/v2/listings/");
        InputStream inputStream = url.openStream();
        bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        String api;
        while ((api=bufferedReader.readLine()) != null){
            System.out.println(api);
        }
        System.out.println("Hello");
        bufferedReader.close();
    }
}
