package forEach;

import com.sun.xml.internal.bind.v2.schemagen.xmlschema.ExplicitGroup;

import java.lang.reflect.Array;
import java.util.*;

public class ForEach {
    public static void main(String[] args) {
        int[] array = {2, 3, 99, 77, 87, 56, 77};

        ArrayList<Integer> integers = new ArrayList<>();
        integers.add(0, 2 / 2);
        integers.add(1, 32);
        integers.add(2, 22);
        integers.add(3, 21);
        integers.add(4, 10);
        integers.add(5, 9);
        integers.add(6, 8);
        integers.add(7, 22);
        integers.add(8, 8);
        integers.add(7, 11);

        LinkedList<Integer> linkedList = new LinkedList<>(integers);
        linkedList.addFirst(20);


        Integer integertFirs = linkedList.peekLast();

        HashSet<Integer> hashSet = new HashSet<>(integers);
        Comparator<Person> personComparator = new PersonComparatorName().thenComparing(new PersonComparatorAge());
        TreeSet<Person> treeSet = new TreeSet<>(personComparator);

        treeSet.add(new Person("Vova",23));
        treeSet.add(new Person("Andriy",26));
        treeSet.add(new Person("Maks",25));
        treeSet.add(new Person("Yasha",34));
        treeSet.add(new Person("Yasha",26));
        treeSet.add(new Person("Vova",10));

        double b1 = 1.6666;

        int b2 = (int)Math.round(b1);
        System.out.println(b2);
        Person person = new Person("Vova",23);


    }
}

class Person {
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age &&
                Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, age);
    }
}

class PersonComparatorName implements Comparator<Person> {

    public int compare(Person a, Person b) {

        return a.getName().compareTo(b.getName());
    }
}

class PersonComparatorAge implements Comparator<Person> {

    public int compare(Person a, Person b) {

        if (b.getAge() > a.getAge()) {
            return -1;
        }
        if (b.getAge() < a.getAge()) {
            return 1;
        }
        else return 0;
    }
}
