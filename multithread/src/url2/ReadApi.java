package url2;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class ReadApi {
    public static void main(String[] args) throws IOException {
        URL url = new URL("https://api.coinmarketcap.com/v2/ticker/?limit=10");
        InputStream inputStream = url.openStream();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        String myApi = null;
        int myApiInt;

        while ((myApiInt = inputStreamReader.read()) != -1){
            myApi += String.valueOf(Character.toChars(myApiInt));
            System.out.println(myApi);

        }
        inputStreamReader.close();
        System.out.println("SECOND VARIANT");

    }
}
